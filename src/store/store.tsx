import React, { createContext, useReducer } from 'react';

import combineReducers from 'react-combine-reducers';

const [rootReducerCombined, initialStateCombined] = combineReducers(
	{
        // identity: [identityReducer, initialIdentityState],
		
	});

export const Store = createContext(initialStateCombined);

export const StoreProvider = (props: any) => {
	const [state, dispatch] = useReducer(rootReducerCombined, initialStateCombined);
	const value = { state, dispatch };
	
	return (<Store.Provider value={value}>{props.children}</Store.Provider>);
}
