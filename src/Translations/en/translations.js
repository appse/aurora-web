export const TRANSLATIONS_EN = {
  login:"login",
  page_not_found : "Page not found",
  title1: "Safe & secure",
  subheading1: "Communicate with your team, confident \n that your messaging is stored securely",
  title2: "You're in control",
  subheading2: " Keep control of your data in the cloud,\n or via on-site storage",
  title3: " Seamless",
  subheading3: "  Send chats, documents, images, video \n and voice recordings instantly",
  title4: "Welcome abroad",
  subheading4: " Select an option below to \n get started",
  signupreq : "Sign-up with work email",
  signup: "Sign-up",
  signup_subheading: "Use your work email to \n create an account",

  next: 'Next',
  skip: 'Skip',
  login: 'Login'
 };