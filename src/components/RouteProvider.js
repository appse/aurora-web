import React, {useState } from "react";

import { BrowserRouter,
    Routes,
    Route } from "react-router-dom";
import Login from "../pages/Login/login.jsx";
import {Welcome1, Welcome2 , Welcome3} from "../pages/OnBoarding/index.jsx.js";
 

const RoutesProvider = () => {
    return (
     <>
    <BrowserRouter>
        <Routes>
            <Route path="/" element={<Welcome1 />}>
              
                <Route path="welcome2" element={<Welcome2 />} />  
                <Route path="welcome3" element={<Welcome3 />} /> 
                <Route path="login" element={<Login />} /> 
                <Route path="*" element={
                               <main style={{ padding: "1rem" }}>
                                  <p>There's nothing here!</p>
                                </main> }  /> 
            </Route>
        </Routes>
    </BrowserRouter>
    </>
   
);
}

export default RoutesProvider;
