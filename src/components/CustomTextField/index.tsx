import { Input,  } from "native-base";
import { useField } from 'formik';

export const CustomTextField = ({ name, ...props }: any) => {
  const [field, meta] = useField(name!);
  return <Input
    id={field.name}
    name={field.name}
    helperText={meta.touched ? meta.error : ''}
    error={meta.touched && Boolean(meta.error)}
    value={field.value}
    onChange={field.onChange}
    {...props}
  />
}