import React from 'react';
import {
  Box, 
  useColorMode,
  VStack,

} from "native-base";
import { StoreProvider } from './store/store';

import { BrowserRouter,  Routes, Route } from "react-router-dom";
import Layout from './pages/Layout';
import OnBoarding1 from "./pages/OnBoarding/onBoarding1.jsx";
import OnBoarding2 from "./pages/OnBoarding/onBoarding2.jsx";
import OnBoarding3 from "./pages/OnBoarding/onBoarding3.jsx";
import OnBoarding4 from "./pages/OnBoarding/onBoarding4.jsx";
import SignUp1 from './pages/Signup/signUp1';

import NotFound from './pages/NotFound';

import Login from "./pages/Login/login.jsx";


const App : React.FC = () => {
  const { colorMode } = useColorMode();

  return (
    <Box
      bg={colorMode === "light" ? "coolGray.50" : "coolGray.900"}
      minHeight="100vh"
      justifyContent="center"
      px={4}
      py={5}
    >
      <VStack space={5} alignItems="center">
      <StoreProvider>
        <Routes>
            <Route path="/" element={<Layout />}>
                <Route index  element={<OnBoarding1 />} />              
                <Route path="2" element={<OnBoarding2 />} />  
                <Route path="3" element={<OnBoarding3 />} /> 
                <Route path="4" element={<OnBoarding4 />} /> 
                <Route path="login" element={<Login />} /> 
                <Route path="signup1" element={<SignUp1 />} /> 
                <Route path="*" element={
                               <NotFound /> }  /> 
            </Route>
        </Routes>
        </StoreProvider>
  
      
      
      </VStack>
    </Box>
  );
}



export default App;
