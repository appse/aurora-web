 import Login from '../pages/Login/login'
// import Dashboard from '../Pages/Dashboard'
import NotFound from '../pages/NotFound'
import Welcome from '../pages/OnBoarding'

const routes =[
  {
    path:'/',
    element: Welcome,
    isPrivate: false,
  },
  {
    path:'/login',
    element: Login,
    isPrivate: false,
  },

  {
    path:'/*',
    element: NotFound,
    isPrivate: true,
  },

 
]
 
export default routes