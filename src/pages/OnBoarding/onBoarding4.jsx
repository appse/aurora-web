import React from 'react';
import { Box, Heading, Center,  VStack, Button, Text} from "native-base";
import styles from './onBoarding.module.css';
import { useNavigate } from "react-router-dom";
import { useTranslation } from 'react-i18next';

const OnBoarding4 = () => {
    let navigate = useNavigate();
    const { t } = useTranslation();
  const handleSignIn = () => {
    navigate("../signup1", {replace: true});
  }

   const handleLogin = () => { 
      navigate("../login", { replace: true });
   }
    return (
      <Box width={'sm', 'md'} flex={1} bg='white' alignItems='center' rounded = "2xl" bg={'white.50'}>
      <Center  mt={7}>
        <VStack w='100%' alignItems='center' space={4}>
          <VStack alignItems='center'>
            <Heading size='xl'>
            {t('title4')}!
            </Heading>
            <Heading size="xs" textAlign='center'>
           {t('subheading4')}.
            </Heading>
          </VStack >
          <VStack alignItems='center'>
              <img src = {require("../../assets/welcome4.png")} alt='Welcome' />
          </VStack>
             <VStack space={2} mb={7}>
             <Button width={80} height={70} size="md" bg={'primary.450'} onPress={(e) => handleSignIn(e)}>               
              <Text fontWeight={700} color={'white.50'} fontSize="md"> {t('signupreq')}</Text>  
              </Button>
              <Button width={80} height={70} size="md" bg={'primary.450'} onPress={(e) => handleLogin(e)}>
              <Text fontWeight={700} color={'white.50'} fontSize="md">{t('login')}</Text>  
              </Button>
            
            </VStack>
        </VStack>
      </Center>     
  </Box>
      );
    };

export default OnBoarding4;