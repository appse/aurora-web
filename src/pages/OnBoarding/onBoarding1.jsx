import React,  {useState} from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { VStack, Center, Button, Text, HStack, Box, Heading, Circle} from "native-base";
import styles from './onBoarding.module.css';
import { useNavigate, Outlet} from "react-router-dom";
import { useTranslation } from 'react-i18next';

function OnBoarding1 () {
  let navigate = useNavigate();
  const { t } = useTranslation();

    function handleSkip (e) {
      navigate("../4", { replace: true });
    }

    function handleNext (event) {
      event.preventDefault();
      navigate("../2", { replace: true });
    }

    return (
        <Box width={'sm', 'md'} bg='white' alignItems='center' rounded = "2xl" bg ={'white.50'}>
        <Center  mt={7}>
          <VStack w='100%' alignItems='center' space={4}>
            <VStack alignItems='center'>
              <Heading size='xl'>
              {t('title1')}.
              </Heading>
              <Heading size="xs" textAlign='center'>
              {t('subheading1')}. 
              </Heading>
              <div>
                  <HStack mt = {4} space={3}>
                  <Circle onClick={(e) => handleNext (e)} size="8px" bg={'primary.450'}> 
                </Circle>
                  <Circle onClick={() => console.log('Clicked')} size="8px" bg={'primary.300'}> 
                </Circle>
                  <Circle onClick={() => console.log('Clicked')} size="8px" bg={'primary.300'}>
                </Circle>
                  </HStack>
              </div>
            </VStack >
            <VStack alignItems='center'>
                <img src = {require("../../assets/welcome1.png")} alt='Welcome' />
            </VStack>
               <VStack space={2} mb={7}>
              <Button width={80} height={70} size="md" bg={'primary.450'} onPress={(e) => handleNext(e)}>               
              <Text fontWeight={700} color={'white.50'} fontSize="md">{t('next')}</Text>  
              </Button>
              <Button width={80} height={70} size="md" bg={'primary.450'} onPress={(e) => handleSkip(e)}>
              <Text fontWeight={700} color={'white.50'}  fontSize="md">{t('skip')}</Text>  
              </Button>
            </VStack>
          </VStack>
        </Center>     
    </Box>
    );

}

export default OnBoarding1;

// https://react-responsive-carousel.js.org/storybook/?path=/story/02-advanced--with-external-controls