import React from 'react';
import { Box, Heading, Center,  VStack} from "native-base";
import styles from './login.module.css'
import { useNavigate } from "react-router-dom";

const Login = () => {
  let navigate = useNavigate();

  const handleSignIn = () => {

  }

   const handleLogin = () => { 
       console.log('login pressed')
   }
    return (
      <Box flex={1} bg='white' alignItems='center' rounded = "lg" bg ={'white.50'}>
      <Center  mt={3}>
        <VStack w='100%' alignItems='center' space={6}>
          <VStack alignItems='center'>
            <Heading size='xl'>
            Welcome abroad!
            </Heading>
            <Heading size="xs" textAlign='center'>
            Select an option below to{'\n'}get started.
            </Heading>
          </VStack >
          <VStack alignItems='center'>
              <img src = {require("../../assets/welcome4.png")} alt='Welcome' />
         </VStack>
         </VStack>
      </Center>     
  </Box>
      );
    };

export default Login;