import React,  {useState} from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { VStack, Center, Button, Text, Flex, Box, Heading, ArrowBackIcon } from "native-base";

import { useNavigate} from "react-router-dom";
import { useTranslation } from 'react-i18next';

function SignUp1 () {
  let navigate = useNavigate();
  const { t } = useTranslation();

    function handleSkip (e) {
      navigate("../4", { replace: true });
    }

    function handleNext (event) {
      event.preventDefault();
      navigate("../2", { replace: true });
    }
// <ArrowBackIcon  size="4" />
    return (
        <ArrowBackIcon  size="4" />
  
    );

}

export default SignUp1;

