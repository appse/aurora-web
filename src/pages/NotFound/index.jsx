// Pages/NotFound/index.jsx

import { Box } from "native-base";
import { useTranslation } from 'react-i18next';

function NotFound() {
  const { t } = useTranslation();

    return (
    <Box>      
      <Box alignSelf="center" bg="primary.500"   maxW = "80"
        rounded = "lg"  _text={{
        fontSize: "md",
        fontWeight: "medium",
        color: "warmGray.50",
        letterSpacing: "lg",
      
        }}>
         {t('page_not_found')} 
      </Box>
    </Box>
    );
 

  
}

export default NotFound;