import React from "react";
import {render} from "react-dom";
import App from "./App";
import './Translations/i18n';
import { NativeBaseProvider} from "native-base";
import reportWebVitals from "./reportWebVitals";
import Theme from './utility/Theme'
import { BrowserRouter } from "react-router-dom";



render(
  <React.StrictMode>
    <NativeBaseProvider theme={Theme}>
      <BrowserRouter>
      <App />
      </BrowserRouter>     
    </NativeBaseProvider>
  </React.StrictMode>
     ,
 
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
